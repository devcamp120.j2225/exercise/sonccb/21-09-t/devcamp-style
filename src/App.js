import image from './assets/images/48.jpg';
import './App.css';

function App() {
  return (
    <div>
    <div className="dc-container">
      <div>
        <img src={image} alt="avatar" className="dc-image"></img>
      </div>
      <div>
        <p>This is one of the best developer blogs on the planet! I read it daily to improve my skills.</p>
      </div>
      <div className="dc-info">
        <b>Tammy Stevens</b> &nbsp; * &nbsp; Front End Developer
      </div>
    </div>
  </div>

  );
}

export default App;
